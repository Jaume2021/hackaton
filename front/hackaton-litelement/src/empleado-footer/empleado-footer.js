import { LitElement, html } from "lit-element";

class EmpleadoFooter extends LitElement{

    static get properties(){
        return{
        };
    }

    constructor(){
        super();
    }

    render(){
        return html`
            <h1>Footer</h1>
        `;
    }
}

customElements.define('empleado-footer', EmpleadoFooter);